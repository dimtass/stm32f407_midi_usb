/******************** (C) COPYRIGHT 2011 STMicroelectronics ********************
* File Name          : platform_config.h
* Author             : MCD Application Team
* Version            : V3.3.0
* Date               : 21-March-2011
* Description        : Evaluation board specific configuration file.
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PLATFORM_CONFIG_H
#define __PLATFORM_CONFIG_H

#include <stdint.h>
#include <stdio.h>
#include "stm32f4xx.h"
#include "synth_defs.h"
#include "debug_uart.h"

#define DEBUG_TRACE



/**
 * System options
 */
#define TMR_1MSEC
#define TMR_100MSEC
#define TMR_250MSEC
#define TMR_1000MSEC
#define USE_USB_IF		// If its set then USB interface is used instead of UART

/* Declare TX/RX buffers */
#ifdef USE_USB_IF
#define USB_RX_BUFF_SIZE	2048 //VIRTUAL_COM_PORT_DATA_SIZE
#define USB_TX_BUFF_SIZE	2048 //VIRTUAL_COM_PORT_DATA_SIZE
#define UART_RX_BUFF_SIZE	128
#define UART_TX_BUFF_SIZE	4096
#else
#define USB_RX_BUFF_SIZE	64 //VIRTUAL_COM_PORT_DATA_SIZE
#define USB_TX_BUFF_SIZE	64 //VIRTUAL_COM_PORT_DATA_SIZE
#define UART_RX_BUFF_SIZE	2048
#define UART_TX_BUFF_SIZE	1024
#endif

#ifdef DEBUG_TRACE
#define TRACE(X) TRACEL(TRACE_LEVEL_DEFAULT, X)
#define TRACEL(TRACE_LEVEL, X) if (debug_uart_check_trace_level(TRACE_LEVEL)) printf X
#else
#define TRACE(X)
#endif

typedef struct {
	uint16_t ptr_in;
	uint16_t ptr_out;
	uint16_t length;
	uint8_t  int_en;
} tp_buff_pointers;

typedef enum {
	LED_PATTERN_BOOT = 0xFF,
	LED_PATTERN_HEARTBEAT = 0x0F,
} en_led_pattern;

typedef struct {

	/* Timers */
#ifdef TMR_1MSEC
	uint16_t tmr_1ms;
#endif
#ifdef TMR_100MSEC
	uint16_t tmr_100ms;
#endif
#ifdef TMR_250MSEC
	uint16_t tmr_250ms;
#endif
#ifdef TMR_1000MSEC
	uint16_t tmr_1sec;
#endif
	uint8_t tmr_boot_delay;
	uint16_t tmr_fpga_sm_tmr;
	uint16_t tmr_reset;	// When
	/* USB buffers */
	uint8_t usb_tx_ready;
	uint8_t	usb_rx_ready;
	uint8_t	usb_rx_ready_tmr;
	uint8_t usb_rx_buff[USB_RX_BUFF_SIZE];
	tp_buff_pointers usb_rx_buff_p;
	uint8_t usb_tx_buff[USB_TX_BUFF_SIZE];
	tp_buff_pointers usb_tx_buff_p;
	/* UART buffers */
	uint8_t	uart_rx_ready;
	uint8_t	uart_rx_ready_tmr;
	uint8_t uart_rx_buff[UART_RX_BUFF_SIZE];
	tp_buff_pointers uart_rx_buff_p;
	uint8_t uart_tx_buff[UART_TX_BUFF_SIZE];
	tp_buff_pointers uart_tx_buff_p;
	/* system state */
	uint8_t sys_led_pattern;
	uint8_t sys_led_pattern_index;
	uint32_t trace_level;
	/* audio buffers */
	uint16_t audio_buff_0[AUDIO_BUFF_SIZE];
	uint16_t audio_buff_1[AUDIO_BUFF_SIZE];

	uint32_t	phase_accumulator;
	float		play_freq;
} tp_glb;

extern tp_glb glb;

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#define PIN_LED0		GPIO_Pin_9
#define PIN_LED0_PORT	GPIOF
#define PIN_LED1		GPIO_Pin_10
#define PIN_LED1_PORT	GPIOF
#define PIN_KEY0		GPIO_Pin_4
#define	PIN_KEY0_PORT	GPIOE
#define PIN_KEY1		GPIO_Pin_3
#define	PIN_KEY0_PORT	GPIOE
#define PIN_DEBUG1		GPIO_Pin_12
#define PIN_DEBUG2		GPIO_Pin_13
#define	PIN_DEBUG_PORT	GPIOD

#endif /* __PLATFORM_CONFIG_H */

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
