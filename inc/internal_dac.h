/*
 * internal_dac.h
 *
 *  Created on: 22 Apr 2017
 *      Author: dimtass
 */

#ifndef INTERNAL_DAC_H_
#define INTERNAL_DAC_H_

typedef enum {
	AUDIO_CHANNEL_L,
	AUDIO_CHANNEL_R
} en_audio_channel;

void DAC_init(void);

#endif /* INTERNAL_DAC_H_ */
