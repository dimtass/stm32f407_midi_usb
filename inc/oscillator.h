/*
 * oscillator.h
 *
 *  Created on: 20 Apr 2017
 *      Author: dimtass
 */

#ifndef SYNTH_OSCILLATOR_H_
#define SYNTH_OSCILLATOR_H_

#include <stdint.h>

typedef enum {
	OSC_TYPE_SINE,
	OSC_TYPE_SAW,
	OSC_TYPE_PULSE,
} en_osc_type;

typedef struct {
	uint8_t 	type;	//en_osc_type
	float	freq;
	float	phase;
	float	amp;
} tp_oscillator;

void OSC_Init(tp_oscillator * osc, en_osc_type type, float freq);

#endif /* SYNTH_OSCILLATOR_H_ */
