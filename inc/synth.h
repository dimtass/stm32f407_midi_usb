/*
 * synth.h
 *
 *  Created on: 22 Apr 2017
 *      Author: dimtass
 */

#ifndef SYNTH_SYNTH_H_
#define SYNTH_SYNTH_H_

void SYNTH_Init(void);
void SYNTH_calculate(uint16_t * buffer, uint16_t buffer_size, float frequency);

#endif /* SYNTH_SYNTH_H_ */
