/*
 * sinetable.h
 *
 *  Created on: 22 Apr 2017
 *      Author: dimtass
 */

#ifndef SYNTH_SINETABLE_H_
#define SYNTH_SINETABLE_H_

#include <math.h>
#include <stdint.h>
#include "synth_defs.h"

#define _2PI    6.283185307f
#define ALPHA	(WAVETABLE_SIZE/_2PI)

extern const  float_t sinetable[1025];

#endif /* SYNTH_SINETABLE_H_ */
