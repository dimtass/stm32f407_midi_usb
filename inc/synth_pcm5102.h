/*
 * synth.h
 *
 *  Created on: 22 Apr 2017
 *      Author: dimtass
 */

#ifndef SYNTH_PCM5102_H_
#define SYNTH_PCM5102_H_
#include "stm32f4xx.h"
#include "platform_config.h"
#include "i2s.h"


void SYNTH_PCM5102_Init(void);
void SYNTH_PCM5102_calculate(dmabuf_t * buffer, uint16_t buffer_size, float frequency);

#endif /* SYNTH_SYNTH_H_ */
