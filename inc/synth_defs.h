/*
 * synth_defs.h
 *
 *  Created on: 22 Apr 2017
 *      Author: dimtass
 */

#ifndef SYNTH_SYNTH_DEFS_H_
#define SYNTH_SYNTH_DEFS_H_

#include <stdint.h>

#define AUDIO_BUFF_SIZE 	128
#define SAMPLE_RATE			48000 //192000
#define WAVETABLE_SIZE		1024
#define WAVETABLE_FREQ		(SAMPLE_RATE/WAVETABLE_SIZE)


#endif /* SYNTH_SYNTH_DEFS_H_ */
