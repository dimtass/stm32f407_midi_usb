/*
 * midi_notes.h
 *
 *  Created on: 29 Apr 2017
 *      Author: dimtass
 */

#ifndef MIDI_NOTES_H_
#define MIDI_NOTES_H_

#include <stdint.h>

#define MIDI_NOTES_MAX_NUM	128

typedef struct {
	uint8_t		note_id;
	float		freq;
} tp_midi_note;

extern tp_midi_note midi_notes[MIDI_NOTES_MAX_NUM];

void MIDI_Init(void);

#endif /* MIDI_NOTES_H_ */
