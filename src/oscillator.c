/*
 * oscillator.c
 *
 *  Created on: 22 Apr 2017
 *      Author: dimtass
 */
#include "oscillator.h"

void OSC_Init(tp_oscillator * osc, en_osc_type type, float freq)
{
	osc->amp = 1.0f;
	osc->type = type;
	osc->freq = freq;
}

