/*
 * midi_notes.c
 *
 *  Created on: 29 Apr 2017
 *      Author: dimtass
 */
#include "midi_notes.h"
#include <math.h>

tp_midi_note midi_notes[MIDI_NOTES_MAX_NUM] = {0};

void MIDI_Init(void)
{
	for (int i=0; i<MIDI_NOTES_MAX_NUM; i++) {
		midi_notes[i].note_id = 0;
		/* Calculate the freq of the midi notes
		 * see here: https://en.wikipedia.org/wiki/Piano_key_frequencies
		 * f(n) = pow(sqrt(2,12), i-49)*440
		 */
		midi_notes[i].freq =powf(1.05946, i-49)*440;
	}
}
