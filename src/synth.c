/*
 * synth.c
 *
 *  Created on: 22 Apr 2017
 *      Author: dimtass
 */

#include "platform_config.h"
#include "synth_defs.h"
#include "sinetable.h"
#include "fix32.h"

static uint32_t desiredFreq=1000;    // desired generated frequency
static uint32_t phaseAccumulator=0; // fixed-point (16.16) phase accumulator

#define _2PI            6.283185307f
#define _PI             3.14159265f
#define TABLE_SIZE 		1024						// size of wavetable
#define TABLE_FREQ		(SAMPLE_RATE / TABLE_SIZE)	// frequency of one wavetable period
static float sine[TABLE_SIZE+1];	// +1 for interpolation
#define BIT_COUNTER		32

// Q16.16
#define FRAC		16
#define Q16(X)		(X * (float)(1<<FRAC))
#define QFLOAT(X)	(X / (float)(1<<FRAC))
#define Q16_TO_UINT(X)	((uint32_t)X / (1<<FRAC))

void sine_init(void)
{
	// populate table table[k]=sin(2*pi*k/N)
	for(int i = 0; i < TABLE_SIZE; i++) {
		// calculating sine wave
		sine[i] = sinf(_2PI * ((float)i/TABLE_SIZE));
	}
	/* set the last byte equal to first for interpolation */
	sine[TABLE_SIZE] = sine[0];
	glb.phase_accumulator = 0;
}

void SYNTH_Init(void)
{
	sine_init();
}

void SYNTH_calculate(uint16_t * buffer, uint16_t buffer_size, float frequency)
{
	uint32_t phaseIncrement = Q16((float)frequency*TABLE_SIZE / SAMPLE_RATE);
	uint32_t index = 0;

	for(int i=0; i<buffer_size; i++)
    {
		/* Increment the phase accumulator */
		phaseAccumulator += phaseIncrement;

		phaseAccumulator &= TABLE_SIZE*(1<<16) - 1;

		index = phaseAccumulator >> 16;
//		index = fix32_trun(phaseAccumulator, 16);

		/* interpolation */
		float v1 = sine[index];
		float v2 = sine[index+1];
		float fmul = (phaseAccumulator & 65535)/65536.0f;
//		float fmul = fix32_frac(phaseAccumulator, 16);
		float out = v1 + (v2-v1)*fmul;

		buffer[i] = (uint16_t)(2048 * out + 2047) ;
    }
}
