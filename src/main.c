/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include <string.h>
#include <stdlib.h>

#include "stm32f4xx.h"
#include "platform_config.h"
#include "hw_config.h"
#include "internal_dac.h"
#include "synth.h"
#include "synth_pcm5102.h"
#include "midi_notes.h"
#include "i2s.h"

// Midi
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "usbd_midi_core.h"
#include "usbd_midi_user.h"

tp_glb glb = {0};
__ALIGN_BEGIN USB_OTG_CORE_HANDLE  USB_OTG_dev __ALIGN_END;


void cmd_handler(uint8_t *buffer, size_t bufferlen, uint8_t sender)
{
	if (!strncmp((char*)buffer, "MIDI_NOTE=", 10)) {
		uint8_t note_id = strtoul((char*) &buffer[4], NULL, 10);
		glb.play_freq = midi_notes[note_id].freq;
		TRACE(("Set midi note:%d and freq:%f\n", note_id, glb.play_freq ));
	}
	else if (!strncmp((char*)buffer, "FREQ=", 5)) {
		glb.play_freq = strtof((char*) &buffer[5], NULL);
		TRACE(("Set frequency to: %f\n", glb.play_freq ));
	}
}

void main_loop(void)
{
	debug_uart_rx_poll();
}

int main(void)
{
	/* SysTick timer */
	if (SysTick_Config(SystemCoreClock / 1000)) {
		while (1){};	// Capture error
	}
	/* set trace levels */
	glb.trace_level =
			0
			| TRACE_LEVEL_DEFAULT
//			| TRACE_LEVEL_USB
			;

	/* Init Data Watchpoint and Trace */
	DWT_Init();

	/* Configure GPIOs */
	GPIO_Configuration();

    /* Initialize the midi driver */
	usbd_midi_init_driver(&MIDI_fops);
    /* Make the connection and initialize to USB_OTG/usbdc_core */
	USBD_Init(&USB_OTG_dev,
	            USB_OTG_FS_CORE_ID,
	            &USR_desc,
				&USBD_MIDI_cb,
	            &USR_cb);

	debug_uart_init(USART1, 115200, glb.uart_rx_buff, UART_RX_BUFF_SIZE, glb.uart_tx_buff, UART_TX_BUFF_SIZE, &cmd_handler);
	debug_uart_set_trace_level(TRACE_LEVEL_DEFAULT, 1);

	glb.play_freq = 1000;

	/* Configure DACs */

	TRACE(("Init DACs\n"));

//	SYNTH_Init();
//	DAC_init();

//	LDataSend.W=0x1b2c3d4e;  // data to send  from buffer	  3D4E 1B2C
//	RDataSend.W=0xA1A25354;							 //   5354 A1A2
//	// fill array
//	for(int i=0;i<DMARCVBUF_SIZE;i++){
//		DMATRMBUF_0[i].LEFT.W16 =__REV16(__REV((LDataSend.W)));
//		DMATRMBUF_0[i].RIGHT.W16=__REV16(__REV((RDataSend.W)));
//		DMATRMBUF_1[i].LEFT.W16 =__REV16(__REV((LDataSend.W)));
//		DMATRMBUF_1[i].RIGHT.W16=__REV16(__REV((RDataSend.W)));
//	}

	SYNTH_PCM5102_Init();
	InitI2S3();

	TRACE(("Program started.\n"));
	TRACE(("System core clk: %lu\n", SystemCoreClock));

	while(1) {
		main_loop();
	}
}


/**
 * This is a (weak) function in syscalls.c and is used from printf
 * to print data to the UART1
 */
int __io_putchar(int ch)
{
	debug_uart_send(ch);
	return ch;
}

size_t send_string(uint8_t * buffer, size_t length)
{
	size_t i = 0;
	for (i=0; i<length; i++) {
		__io_putchar(buffer[i]);
	}
	return(i);
}


